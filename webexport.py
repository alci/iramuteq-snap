import codecs
from shutil import copyfile
import os
from chemins import PathOut



class WebExport :
    def __init__(self, parent, parametres) :
        self.parent = parent
        self.parametres = parametres
        self.jspathin = os.path.join(self.parent.AppliPath, 'webexport','js')
        self.csspathin = os.path.join(self.parent.AppliPath, 'webexport', 'css')
        if not 'dirout' in self.parametres :
            self.pathout = PathOut(filename = self.parametres['gexffile'], analyse_type='webexport')
            dirout = self.pathout.mkdirout()
            self.pathout.dirout = dirout
        else :
            self.pathout = PathOut(dirout = self.parametres['dirout'])
        self.makedirout()

    def makedirout(self) :
        jss = os.listdir(self.jspathin)
        css = os.listdir(self.csspathin)
        jspathout = os.path.join(self.pathout.dirout, 'js')
        cssout =  os.path.join(self.pathout.dirout, 'css')
        os.makedirs(jspathout)
        os.makedirs(cssout)
        for f in jss :
            copyfile(os.path.join(self.jspathin, f), os.path.join(jspathout, f))
        for f in css :
            copyfile(os.path.join(self.csspathin, f), os.path.join(cssout, f))

    def exportafc(self) :
        copyfile(self.parametres['gexffile'], os.path.join(self.pathout.dirout, os.path.basename(self.parametres['gexffile'])))
        afcfile = os.path.join(self.parent.AppliPath, 'webexport', 'afc.html')
        afcout = os.path.join(self.pathout.dirout, 'afc.html')
        with codecs.open(afcfile, 'r', 'utf8') as f :
            content = f.read()
        self.parametres['gexffile'] = os.path.basename(self.parametres['gexffile'])
        content = content % self.parametres
        with open(afcout, 'w') as f :
            f.write(content)
        return afcout
        
    def exportsimi(self) :
        simifile = os.path.join(self.parent.AppliPath, 'webexport', 'graphe.html')
        simiout = os.path.join(self.pathout.dirout, 'graphe.html')
        with codecs.open(simifile, 'r', 'utf8') as f :
            content = f.read()
        self.parametres['gexffile'] = os.path.basename(self.parametres['gexffile'])
        content = content % self.parametres
        with open(simiout, 'w') as f :
            f.write(content)
        return simiout             
        